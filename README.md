# README

# Requirements:
Part 1 - Adding a Cashier to the restaurant

* Prices of the food are on the menu.
* Customers can still order only one item from the menu. The customer better have enough money to pay for his item.
* The waiter will � what?  How do bills get to cashier? [We�ll talk about design in class].
* The customer will � what? How do customers pay. [We�ll talk about design in class].
 
Part 2 � The Cook may run out of food. Here are some requirement details:

* The Cook has a source for food called Market. [Treat the Market as an agent.]
* When inventory for items is low, they should be ordered from the Market (the order might take hours to be fulfilled).
* Customers should be given a chance to reorder for an item that has run out.
* The Market will then � what? [We�ll talk about design in class.]
 
In non-normative scenario, Market may be unable to fulfill an order and needs to send a message stating state. Cook has multiple markets.
 
Part 3 � The Waiter can go on break.

* The waiter has a GUI button for going on a break.
* When the button is pushed, the waiter does ... what? [We'll talk about the design in class.]
* What role does the host play? [We'll talk about the design in class.]
 
Part 4 � Some more Customer scenarios.

* If all the tables are full, the host tells the customer who may decide to leave.
* Customer leaves because everything is too expensive.