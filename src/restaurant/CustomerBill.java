package restaurant;

public class CustomerBill extends Bill
{
	public WaiterAgent waiter;
	public CustomerAgent customer;
	public int table;
	
	public CustomerBill(WaiterAgent waiter, CustomerAgent customer, int table)
	{
		super();
		this.waiter = waiter;
		this.customer = customer;
		this.table = table;
	}
	
}
