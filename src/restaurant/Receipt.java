package restaurant;

public class Receipt 
{
	private String choice;
	private int change;

	public Receipt(String choice, int change)
	{
		this.choice = choice;
		this.change = change;
	}
	
	public int takeChange()
	{
		int chg = change;
		this.change = 0;
		return chg;
	}
	
	public String getChoice()
	{
		return choice;
	}
}
