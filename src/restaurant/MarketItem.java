package restaurant;

public class MarketItem 
{
	private String name;
	private int quantity;
	private int unitPrice;
	
	public MarketItem(String name, int quantity, int unitPrice)
	{
		this.name = name;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
	}
	
	public MarketItem(String name, int unitPrice)
	{
		this(name, 0, unitPrice);
	}
	
	public void addUnits(int quantity)
	{
		this.quantity += quantity;
	}
	
	public int getQuantity()
	{
		return quantity;
	}
	
	public String getName()
	{
		return name;
	}
	
	/**
	 * Finds how many units would be removed by removeUnits() without actually
	 * removing them.
	 * @param quantityDesired
	 * @return Number of units that would be removed
	 */
	public int testRemoveUnits(int quantityDesired)
	{
		int quantityRemoved = 0;
		
		if (quantityDesired > quantity)
			quantityRemoved = quantity;
		else
			quantityRemoved = quantityDesired;
		
		return quantityRemoved;
	}
	
	/**
	 * Removes as many units as possible up (including) the desired number without 
	 * letting the inventory go below zero.
	 * @param quantityDesired
	 * @return A MarketItem object containing the items removed.
	 */
	public MarketItem removeUnits(int quantityDesired)
	{
		int quantityRemoved = testRemoveUnits(quantityDesired);
		
		quantity -= quantityRemoved;

		return new MarketItem(this.name, quantityRemoved, this.unitPrice);
	}
	
	public int getPrice(int numUnits)
	{
		return unitPrice*numUnits;
	}
	
	/**
	 * Returns true if both MarketItems have the same name.
	 */
	public boolean equals(Object compareTo)
	{
		if (compareTo instanceof MarketItem)
		{
			return this.name.equals(( (MarketItem)compareTo ).getName());
		} 
		
		return false;
	}
}





























