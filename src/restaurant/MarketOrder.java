package restaurant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class MarketOrder
{
	private TreeMap<String, Integer> orderItems;
	private ArrayList<MarketItem> shipmentManifest;
	public enum MarketOrderState { PENDING, READY_TO_SHIP, SHIPPING, SHIPPED };
	protected MarketOrderState state;
	public MarketBill bill;
	
	public CookAgent cook;
	public MarketAgent market;
	public CashierAgent cashier;
	
	public MarketOrder(CookAgent cook, CashierAgent cashier, MarketAgent market)
	{
		this.cook = cook;
		this.market = market;
		this.cashier = cashier;
		
		orderItems = new TreeMap<String, Integer>();
		shipmentManifest = null;
		state = MarketOrderState.PENDING;
		bill = new MarketBill(market);
	}

	public void addItemToOrder(String choice, int quantity) 
	{
		if (orderItems.containsKey(choice))
			orderItems.put(choice, orderItems.get(choice) + quantity); // Add the new quantity to the old
		else
			orderItems.put(choice,  quantity);
	}
	
	public void addItemToShipment(MarketItem item)
	{
		if (shipmentManifest == null)
			shipmentManifest = new ArrayList<MarketItem>();
		
		bill.addItem(item.getName(), item.getQuantity());
		
		boolean shipmentAlreadyContainsItem = false;
		
		for (MarketItem shipmentItem : shipmentManifest)
		{
			
			if (shipmentItem.getName().equals(item.getName()))
			{
				shipmentAlreadyContainsItem = true;
				shipmentItem.addUnits(item.getQuantity()); // Add the new quantity to the old
				break;
			}
			
			if (!shipmentAlreadyContainsItem)
				shipmentManifest.add(item);
		}
	}

	public void calculateBill(HashMap<String, Integer> prices) 
	{
		bill.calculate(prices);
	}
	
	public MarketOrderState getState()
	{
		return state;
	}
	
	public ArrayList<MarketItem> getShipmentManifest()
	{
		return shipmentManifest;
	}
	
	public TreeMap<String, Integer> getOrderItems()
	{
		return orderItems;
	}
	
	public double getShippingTime()
	{
		double shippingTime = 10;
		
		for (MarketItem mi : shipmentManifest)
			shippingTime += 0.1*mi.getQuantity();
		
		return shippingTime;
	}
}























