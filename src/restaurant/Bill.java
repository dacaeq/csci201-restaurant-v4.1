package restaurant;

import java.util.HashMap;
import java.util.TreeMap;

public class Bill implements BillModel
{
	public TreeMap<String, MarketItem> orderItems; // Quantity of each choice ordered
	private int charge; // Amount charged by billing party
	private int payAmt; // Amount paid by bill recipient

	public enum BillState { UNCALCULATED, PENDING, UNPAID, PAID };
	public BillState state;

	public Bill()
	{
		orderItems = new TreeMap<String, MarketItem>();
		state = BillState.UNCALCULATED;
		charge = -1;
		payAmt = -1;
	}

	public boolean addItem(String choice, int quantity)
	{
		// Check if the bill has been calculated yet
		if (state != BillState.UNCALCULATED)
			return false;

		if (orderItems.containsKey(choice))
			orderItems.get(choice).addUnits(quantity); // Add the new quantity to the old
		else
			orderItems.put(choice,  new MarketItem(choice, quantity, 0));

		return true;
	}

	public void calculate(final HashMap<String, Integer> prices)
	{
		charge = 0; 

		for (String choice : orderItems.keySet())
		{
			charge += orderItems.get(choice).getQuantity()*prices.get(choice);
		}

		state = BillState.PENDING;
	}

	public int checkCharge()
	{
		if (state == BillState.UNCALCULATED)
			System.err.println("Tried to check the charge on an uncalculated bill!");
		return charge;
	}

	/**
	 * Call to pay the bill. Set the return = to your total money.
	 * Ex wallet contains money, want to pay with $5 bill: money = bill.pay(money, 5) 
	 * @param wallet - All the money you own
	 * @param payAmt - How much you want to use to pay
	 * @return The amount that will be left in your wallet
	 */
	public int pay(int wallet, int payAmt)
	{
		this.payAmt = payAmt;
		if (payAmt >= charge)
		{
			this.state = Bill.BillState.PAID;
			return wallet - payAmt;
		} else {
			this.state = Bill.BillState.UNPAID;
			return wallet;
		}
	}

	/**
	 * Gives the amount paid by the bill recipient.
	 * @return The amount paid by the bill recipient.
	 */
	public int getPayment()
	{
		return this.payAmt;
	}
}





























