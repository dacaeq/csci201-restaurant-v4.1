package restaurant;

import java.util.ArrayList;

import agent.Agent;

public class CashierAgent extends Agent
{
	ArrayList<Bill> bills;
	int wallet;
	Menu menu;
	
	public CashierAgent(Menu menu)
	{
		super();
		bills = new ArrayList<Bill>();
		wallet = 100; // Start with $100
		this.menu = menu;
	}
	
	// ** MESSAGING ** \\
	public synchronized void msgNeedBill(WaiterAgent waiter, CustomerAgent customer, int table, String choice)
	{
		CustomerBill bill = new CustomerBill(waiter, customer, table);
		bill.addItem(choice, 1);
		bill.calculate(menu.defaultPrices);
		System.out.println("Cashier created a new bill for customer " + customer.getName() + " charging $" + bill.checkCharge());
		bills.add(bill);
		stateChanged();
	}
	
	public synchronized void msgHereIsBill(Bill bill)
	{
		bills.add(bill);
		stateChanged();
	}
	
	// ** SCHEDULER ** \\
	protected boolean pickAndExecuteAnAction() 
	{
		Bill bill = null;
		
		bill = findBillWithState(CustomerBill.class, Bill.BillState.UNPAID);
		if (bill != null) { this.nonPayingCustomer(bill); return true; }
		
		bill = findBillWithState(CustomerBill.class, Bill.BillState.PENDING);
		if (bill != null) { this.giveBill(bill); return true; }
		
		bill = findBillWithState(CustomerBill.class, Bill.BillState.PAID);
		if (bill != null) { this.processBill(bill); return true; }
		
		bill = findBillWithState(MarketBill.class, Bill.BillState.PENDING);
		if (bill != null) { this.processBill(bill); return true; }
		
		return false;
	}
	
	private Bill findBillWithState(Class billClass, Bill.BillState state)
	{
		synchronized(this)
		{
			for (Bill b : bills)
			{
				if (billClass.equals(CustomerBill.class) && b instanceof CustomerBill)
				{
					CustomerBill cb = (CustomerBill)b;
					if (cb.state == state) return cb;
				}

				if (billClass.equals(MarketBill.class) && b instanceof MarketBill)
				{
					MarketBill mb = (MarketBill)b;
					if (mb.state == state) return mb;
				}

			}
		}

		return null;
	}
	
	// ** ACTIONS ** \\
	private void processBill(Bill bill)
	{
		if (bill instanceof CustomerBill)
		{
			CustomerBill cbill = (CustomerBill)bill;
			
			if (cbill.getPayment() < cbill.checkCharge())
			{
				cbill.state = Bill.BillState.UNPAID;
				stateChanged();
				return;
			}
			
			wallet += cbill.getPayment();
			String choice = cbill.orderItems.keySet().iterator().next();
			cbill.customer.msgThankYouForComing(new Receipt(choice, cbill.getPayment() - menu.getPriceOf(choice)));
			
			System.out.println("Cashier processed bill for customer " + cbill.customer.getName());
			System.out.println("Cashier now has $" + wallet);
			bills.remove(cbill);
			stateChanged();
		}
		
		if (bill instanceof MarketBill)
		{
			MarketBill mbill = (MarketBill)bill;
			wallet = mbill.pay(wallet, mbill.checkCharge());
			
			mbill.market.msgHereIsPayment(mbill);
			
			System.out.println("Cashier processed bill for market.");
			System.out.println("Cashier now has $" + wallet);
			bills.remove(mbill);
			stateChanged();
		}
	}
	
	private void giveBill(Bill b)
	{
		if (b instanceof CustomerBill)
		{
			CustomerBill bill = (CustomerBill)b;
			
			bill.waiter.msgBillReady(bill);
			bills.remove(bill);
			stateChanged();
		}
	}
	
	private void nonPayingCustomer(Bill b)
	{
		if (b instanceof CustomerBill)
		{
			CustomerBill bill = (CustomerBill)b;
			
			bill.customer.msgWashDishes(bill.checkCharge()*2);
			bills.remove(bill);
			stateChanged();
		}
	}
}


































