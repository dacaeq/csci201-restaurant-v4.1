package restaurant.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;

import restaurant.CookAgent;

public class CookPanel extends JPanel implements ActionListener
{
	final private int panelWidth = 150;
	final private int panelHeight = 200;
	
	private JScrollPane scrollPane;
	private JPanel panel;
	private ArrayList<InventorySlot> inventoryPanels = new ArrayList<InventorySlot>();
	
	private CookAgent cook;
	
	private Timer timer;
	
	public CookPanel()
	{
		panel = new JPanel();
		
		this.setPreferredSize(new Dimension(panelWidth, panelHeight));
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(new JLabel("Cook inventory"));
		
		panel.setPreferredSize(new Dimension(panelWidth, panelHeight));
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		scrollPane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setViewportView(panel);
		this.add(scrollPane);
		
		timer = new Timer(100, this);
		timer.setActionCommand("refresh");
		timer.start();
		
	}
	
	private class InventorySlot extends JPanel implements ActionListener
	{
		JButton add, sub;
		JLabel name, quantity;
		CookPanel cookPanel;
		
		public InventorySlot(String name, int quantity, CookPanel cookPanel)
		{
			this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
			this.name = new JLabel(name);
			this.quantity = new JLabel(quantity + "");
			
			this.cookPanel = cookPanel;
			
			add = new JButton("+");
			add.addActionListener(this);
			sub = new JButton("-");
			sub.addActionListener(this);
			
			this.add(this.name);
			this.add(new JLabel(": "));
			this.add(this.quantity);
			this.add(add);
			this.add(sub);
		}

		@Override
		public void actionPerformed(ActionEvent e) 
		{
			if (e.getActionCommand().equals("+"))
				cookPanel.changeQuantity(name.getText(), 1);
			
			if (e.getActionCommand().equals("-"))
				cookPanel.changeQuantity(name.getText(), -1);
		}
	}
	
	public void setCook(CookAgent cook)
	{
		this.cook = cook;
		this.fillInventoryList();
	}
	
	private void fillInventoryList()
	{
		if (cook == null)
			return;
		
		HashMap<String, Integer> cookInventory = cook.getInventory();
		
		for (String s : cookInventory.keySet())
		{
			InventorySlot slot = new InventorySlot(s, cookInventory.get(s), this);
			inventoryPanels.add(slot);
			panel.add(slot);
		}
	}
	
	/** Used by InventorySlot class */
	protected void changeQuantity(String item, int delta)
	{
		cook.changeQuantityOfItem(item, delta);
	}
	
	private void refreshInventory()
	{
		if (cook == null)
			return;
		
		HashMap<String, Integer> cookInventory = cook.getInventory();
		
		for (String s : cookInventory.keySet())
		{
			for (InventorySlot slot : inventoryPanels)
			{
				if (s.equals(slot.name.getText()))
				{
					slot.quantity.setText("" + cookInventory.get(slot.name.getText()));
					break;
				}
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getActionCommand().equals("refresh"))
			this.refreshInventory();
	}
}























