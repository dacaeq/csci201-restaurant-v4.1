package restaurant.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;

import restaurant.MarketAgent;
import restaurant.MarketItem;

public class MarketPanel extends JPanel implements ActionListener
{

		final private int panelWidth = 200;
		final private int panelHeight = 200;
		
		private JScrollPane scrollPane;
		private JPanel panel;
		private ArrayList<InventorySlot> inventoryPanels = new ArrayList<InventorySlot>();
		
		private MarketAgent market;
		
		private Timer timer;
		
		public MarketPanel(MarketAgent market)
		{
			this.market = market;
			
			panel = new JPanel();
			
			
			this.setPreferredSize(new Dimension(panelWidth, panelHeight));
			this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			this.add(new JLabel("Market " + this.market.getName() + " inventory"));
			
			panel.setPreferredSize(new Dimension(panelWidth, panelHeight));
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			
			scrollPane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPane.setViewportView(panel);
			this.add(scrollPane);
			
			panel.add(new JLabel(market.getName()));
			
			timer = new Timer(100, this);
			timer.setActionCommand("refresh");
			timer.start();
			
			this.fillInventoryList();
		}
		
		private class InventorySlot extends JPanel implements ActionListener
		{
			JButton add, sub;
			JLabel name, quantity;
			MarketPanel marketPanel;
			
			public InventorySlot(String name, int quantity, MarketPanel marketPanel)
			{
				this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
				this.name = new JLabel(name);
				this.quantity = new JLabel(quantity + "");
				
				this.marketPanel = marketPanel;
				
				add = new JButton("+");
				add.addActionListener(this);
				sub = new JButton("-");
				sub.addActionListener(this);
				
				this.add(this.name);
				this.add(new JLabel(": "));
				this.add(this.quantity);
				this.add(add);
				this.add(sub);
			}

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if (e.getActionCommand().equals("+"))
					marketPanel.changeQuantity(name.getText(), 1);
				
				if (e.getActionCommand().equals("-"))
					marketPanel.changeQuantity(name.getText(), -1);
			}
		}
		
		
		private void fillInventoryList()
		{
			if (market == null)
				return;
			
			ArrayList<MarketItem> marketInventory = market.getInventory();
			
			for (MarketItem item : marketInventory)
			{
				InventorySlot slot = new InventorySlot(item.getName(), item.getQuantity(), this);
				inventoryPanels.add(slot);
				panel.add(slot);
			}
		}
		
		/** Used by InventorySlot class */
		protected void changeQuantity(String item, int delta)
		{
			market.changeQuantityOfItem(item, delta);
		}
		
		private void refreshInventory()
		{
			if (market == null)
				return;

			ArrayList<MarketItem> marketInventory = market.getInventory();

			for (MarketItem item : marketInventory)
			{
				for (InventorySlot slot : inventoryPanels)
				{
					if (item.getName().equals(slot.name.getText()))
					{
						slot.quantity.setText("" + item.getQuantity());
						break;
					}
				}
			}
		}

		@Override
		public void actionPerformed(ActionEvent e) 
		{
			if (e.getActionCommand().equals("refresh"))
			{
				if (inventoryPanels.size() == 0)
					this.fillInventoryList();
				this.refreshInventory();
			}
		}
	}

















