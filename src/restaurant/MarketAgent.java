package restaurant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import restaurant.CookAgent.Status;
import restaurant.MarketOrder.MarketOrderState;

import agent.Agent;

public class MarketAgent extends Agent
{
	private String name;
	
	private ArrayList<MarketOrder> orders;
	private ArrayList<MarketItem> inventory;
	private HashMap<String, Integer> prices = new HashMap<String, Integer>();
	private int wallet;
	
	Timer timer = new Timer();
	
	public MarketAgent(String name)
	{
		super();
		this.name = name;
		
		this.startThread();
		
		orders = new ArrayList<MarketOrder>();
		
		inventory = new ArrayList<MarketItem>();
		inventory.add(new MarketItem("Steak", 20, 5));
		inventory.add(new MarketItem("Chicken", 20, 3));
		inventory.add(new MarketItem("Pizza", 20, 2));
		inventory.add(new MarketItem("Salad", 20, 1));

		prices.put("Steak", 5);
		prices.put("Chicken", 3);
		prices.put("Pizza", 2);
		prices.put("Salad", 1);
		
		wallet = 0;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getMoney()
	{
		return wallet;
	}
	
	/** Used by gui */
	public ArrayList<MarketItem> getInventory()
	{
		return inventory;
	}
	
	public void changeQuantityOfItem(String itemName, int delta)
	{
		for (MarketItem item : inventory)
		{
			if (item.getName().equals(itemName))
			{
				item.addUnits(delta);
			}
		}
	}
	
	public void setPrices(HashMap<String, Integer> prices)
	{
		this.prices = prices;
	}
	
	// ** MESSAGING **
	public void msgOrderSupplies(MarketOrder order)
	{
		orders.add(order);
		stateChanged();
	}
	
	public void msgHereIsPayment(MarketBill bill)
	{
		wallet += bill.getPayment();
		stateChanged();
	}

	protected boolean pickAndExecuteAnAction() 
	{
		if (orders.size() == 0)
			return false;
		
		MarketOrder m = null;
		
		m = findMOWithState(MarketOrderState.READY_TO_SHIP);
		if (m != null) { sendShipment(m); return true; }
		
		m = findMOWithState(MarketOrderState.PENDING);
		if (m != null) { prepareShipment(m); m.state = MarketOrderState.READY_TO_SHIP; return true; }
		
		m = findMOWithState(MarketOrderState.SHIPPED);
		if (m != null) { sendBill(m); notifyShipmentComplete(m); return true; }
		
		return false;
	}
	
	private MarketOrder findMOWithState(MarketOrderState findState)
	{
		for (MarketOrder m : orders)
			if (m.state == findState)
				return m;
		
		return null;
	}

	// ** ACTIONS **
	private void prepareShipment(MarketOrder m)
	{
		TreeMap<String, Integer> orderItems = m.getOrderItems();
		for (String choice : orderItems.keySet())
		{
			for (MarketItem inventoryItem : inventory)
			{
				if (inventoryItem.getName().equals(choice))
				{
					// Remove items from inventory and add to MarketOrder shipment
					m.addItemToShipment(inventoryItem.removeUnits(orderItems.get(choice)));
					break;
				}
			}
		}
		
		m.state = MarketOrderState.READY_TO_SHIP;
	}
	
	private void sendShipment(final MarketOrder m)
	{
		m.cook.msgWillSend(m, (int)m.getShippingTime());
		m.state = MarketOrderState.SHIPPING;
		System.out.println("A shipment is on its way from " + this.name + " and will arrive in " + (int)(m.getShippingTime()) + " seconds.");
		timer = new Timer();
		timer.schedule(new TimerTask(){
			public void run(){//this routine is like a message reception    
				m.state = MarketOrderState.SHIPPED;
				m.cook.msgSuppliesShipped(m.getShipmentManifest(), m);
				orders.remove(m);
				stateChanged();
			}
		}, (int)(m.getShippingTime()*1000));
		
		stateChanged();
	}
	
	private void sendBill(MarketOrder m)
	{
		m.calculateBill(prices);
		m.cashier.msgHereIsBill(m.bill);
		stateChanged();
	}
	
	private void notifyShipmentComplete(MarketOrder m)
	{
		m.cook.msgSuppliesShipped(m.getShipmentManifest(), m);
		stateChanged();
	}
	
	
}


































