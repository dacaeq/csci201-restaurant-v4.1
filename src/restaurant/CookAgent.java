package restaurant;

import agent.Agent;

import java.util.*;
import restaurant.layoutGUI.*;
import java.awt.Color;


/** Cook agent for restaurant.
 *  Keeps a list of orders for waiters
 *  and simulates cooking them.
 *  Interacts with waiters only.
 */
public class CookAgent extends Agent {

	//List of all the orders
	private List<Order> orders = new ArrayList<Order>();
	private Map<String,FoodData> inventory = new HashMap<String,FoodData>();
	public enum Status {PENDING, COOKING, DONE}; // order status

	private ArrayList<MarketAgent> markets = new ArrayList<MarketAgent>();
	private int currentMarketIndex = 0; // Zero indexed
	private ArrayList<MarketOrder> marketOrders = new ArrayList<MarketOrder>();
	private ArrayList<ArrayList<MarketItem>> shipments = new ArrayList<ArrayList<MarketItem>>();
	HashMap<String, Integer> totals = new HashMap<String, Integer>(); // Holds the quantities, taking into account placed market orders

	//Name of the cook
	private String name;

	private CashierAgent cashier;
	private HostAgent host;

	//Timer for simulation
	Timer timer = new Timer();
	Restaurant restaurant; //Gui layout

	/** Constructor for CookAgent class
	 * @param name name of the cook
	 */
	public CookAgent(String name, Restaurant restaurant) {
		super();

		this.name = name;
		this.restaurant = restaurant;
		//Create the restaurant's inventory.
		inventory.put("Steak",new FoodData("Steak", 5, 2, 5, 20));
		inventory.put("Chicken",new FoodData("Chicken", 4, 15, 5, 20));
		inventory.put("Pizza",new FoodData("Pizza", 3, 0, 5, 20));
		inventory.put("Salad",new FoodData("Salad", 2, 0, 5, 20));
		
		// Fill the totals hashmap with the quantities in the inventory hashmap
		for (String key : inventory.keySet())
			totals.put(key, inventory.get(key).quantity);
	}

	public void addMarket(MarketAgent market)
	{
		markets.add(market);
	}

	/** Private class to store information about food.
	 *  Contains the food type, its cooking time, and ...
	 */
	private class FoodData {
		String type; //kind of food
		double cookTime;
		int quantity;
		int low;
		int high;
		// other things ...

		public FoodData(String type, double cookTime, int quantity, int low, int high){
			this.type = type;
			this.cookTime = cookTime;
			this.quantity = quantity;
			this.low = low;
			this.high = high;
		}

		public boolean isLow()
		{
			return (this.quantity <= this.low);
		}
	}
	/** Private class to store order information.
	 *  Contains the waiter, table number, food item,
	 *  cooktime and status.
	 */
	private class Order {
		public WaiterAgent waiter;
		public int tableNum;
		public String choice;
		public Status status;
		public Food food; //a gui variable

		/** Constructor for Order class 
		 * @param waiter waiter that this order belongs to
		 * @param tableNum identification number for the table
		 * @param choice type of food to be cooked 
		 */
		public Order(WaiterAgent waiter, int tableNum, String choice){
			this.waiter = waiter;
			this.choice = choice;
			this.tableNum = tableNum;
			this.status = Status.PENDING;
		}

		/** Represents the object as a string */
		public String toString(){
			return choice + " for " + waiter ;
		}
	}

	/** Used by the GUI to see the cook's inventory */
	public HashMap<String, Integer> getInventory()
	{
		HashMap<String, Integer> foodCount = new HashMap<String, Integer>();

		for (String key : inventory.keySet())
		{
			foodCount.put(key, inventory.get(key).quantity);
		}

		return foodCount;
	}

	/** Used by the GUI to set the cook's inventory */
	public void changeQuantityOfItem(String choice, int delta)
	{
		for (FoodData fd : inventory.values())
			if (fd.type.equals(choice))
				fd.quantity += delta;
	}


	// *** MESSAGES ***

	/** Message from a waiter giving the cook a new order.
	 * @param waiter waiter that the order belongs to
	 * @param tableNum identification number for the table
	 * @param choice type of food to be cooked
	 */
	public void msgHereIsAnOrder(WaiterAgent waiter, int tableNum, String choice){
		orders.add(new Order(waiter, tableNum, choice));
		stateChanged();
	}

	/**
	 * Sent by market to cook.
	 * @param order - what will be shipped
	 * @param shippingDelay - time in seconds for the shipment to arrive
	 */
	public synchronized void msgWillSend(MarketOrder order, int shippingDelay)
	{
		marketOrders.add(order);
		stateChanged();
	}

	public synchronized void msgSuppliesShipped(ArrayList<MarketItem> shipment, MarketOrder mo)
	{
		System.out.println("A shipment from " + this.name + " has arrived at the restaurant!");

		shipments.add(shipment);
		marketOrders.remove(mo);
		stateChanged();
	}


	/** Scheduler.  Determine what action is called for, and do it. */
	protected boolean pickAndExecuteAnAction() 
	{
		// Unpack any shipments
		if (shipments.size() > 0)
		{
			this.unpackShipment(shipments.remove(0));
			return true;
		}

		Order o = null;
		//If there exists an order o whose status is done, place o.
		o = this.findOrderWithState(Status.DONE);
		if (o != null) { placeOrder(o); return true; }

		//If there exists an order o whose status is pending, check the inventory, ordering only if quantities are low, cook o.
		o = this.findOrderWithState(Status.PENDING);
		if (o != null){ checkInventory(true); cookOrder(o); return true; }	

		//we have tried all our rules (in this case only one) and found
		//nothing to do. So return false to main loop of abstract agent
		//and wait.
		return false;
	}

	private Order findOrderWithState(Status orderStatus)
	{
		for (Order o : orders)
			if (o.status == orderStatus)
				return o;

		return null;
	}


	// *** ACTIONS ***

	/** Starts a timer for the order that needs to be cooked. 
	 * @param order
	 */
	private void cookOrder(Order order){
		DoCooking(order);
		inventory.get(order.choice).quantity -= 1;
		totals.put(order.choice, totals.get(order.choice) - 1);
		order.status = Status.COOKING;
	}

	private void placeOrder(Order order){
		DoPlacement(order);
		order.waiter.msgOrderIsReady(order.tableNum, order.food);
		orders.remove(order);
	}

	private void checkInventory(boolean checkIfLow)
	{
		if (markets.size() == 0)
			return;
		
		// Check if quantities are <= low and reorder if they are
		// Take into account any MarketOrder objects in marketOrders
		MarketOrder mo = new MarketOrder(this, this.cashier, markets.get(currentMarketIndex));
		boolean hasItemsToOrder = false;

		for (String key : inventory.keySet())
		{
			FoodData fd = inventory.get(key);

			// Theoretical food data object containing the future quantity once all shipments arrive
			fd.quantity = totals.get(key);
			
			if (!checkIfLow || fd.isLow() || hasItemsToOrder)
			{
				// Will always execute if items are already going to be bought
				// Will always execute if the item is low
				// Will always execute if checkIfLow = false
				hasItemsToOrder = true;
				int difference = fd.high - fd.quantity;
				if (difference > 0)
					mo.addItemToOrder(key, difference);
			}

			if (inventory.get(key).quantity <= 0)
			{
				this.host.msgOutOfChoice(key);

				// See if customer must reorder
				ArrayList<Order> matchOrders = new ArrayList<Order>();
				synchronized(this)
				{
					for (Order o : orders)
					{
						// Find all orders that are ordering the item that is out of stock
						if (o.choice == key && o.status == Status.PENDING)
						{
							matchOrders.add(o);
						}
					}
				}

				if (matchOrders.size() > 0)
				{
					for (Order matchOrder : matchOrders)
					{
						if (matchOrder.choice != null)
						{
							matchOrder.waiter.msgOutOfChoice(matchOrder.tableNum, matchOrder.choice);
							orders.remove(matchOrder);
							stateChanged();
						}
					}
				} // Otherwise, no customers ordered the out of stock item - nobody needs to reorder
			}
		}

		if (hasItemsToOrder)
		{
			// Add the order quantities to the totals quantity
			for (String key : mo.getOrderItems().keySet())
			{
				if (totals.containsKey(key))
				{
					totals.put(key, totals.get(key) + mo.getOrderItems().get(key));
				} else {
					totals.put(key, mo.getOrderItems().get(key));
				}
			}
			// Place the order
			markets.get(currentMarketIndex).msgOrderSupplies(mo);
			System.out.println("Cook just ordered more food from market " + markets.get(currentMarketIndex).getName());
		}

		stateChanged();
	}

	private void unpackShipment(ArrayList<MarketItem> shipment)
	{
		System.out.println("Cook is unpacking a shipment...");
		for (MarketItem mi : shipment)
		{
			// Match each with its corresponding FoodData
			for (String key : inventory.keySet())
			{
				if (mi.getName().equals(key))
				{
					FoodData fd = inventory.get(key);

					// Check if need to notify host that an item is back in stock
					if (fd.quantity <= 0 && mi.getQuantity() > 0)
						host.msgItemInStock(key);

					fd.quantity += mi.getQuantity();
					break;
				}	
			}
		}
		
		shipments.remove(shipment);
	}


	// *** EXTRA -- all the simulation routines***

	/** Returns the name of the cook */
	public String getName(){
		return name;
	}

	private void DoCooking(final Order order){
		print("Cooking:" + order + " for table:" + (order.tableNum+1));
		//put it on the grill. gui stuff
		order.food = new Food(order.choice.substring(0,2),new Color(0,255,255), restaurant);
		order.food.cookFood();

		timer.schedule(new TimerTask(){
			public void run(){//this routine is like a message reception    
				order.status = Status.DONE;
				stateChanged();
			}
		}, (int)(inventory.get(order.choice).cookTime*1000));
	}
	public void DoPlacement(Order order){
		print("Order finished: " + order + " for table:" + (order.tableNum+1));
		order.food.placeOnCounter();
	}


	// Hax
	public void setCashier(CashierAgent cashier)
	{
		this.cashier = cashier;
	}

	public void setHost(HostAgent host)
	{
		this.host = host;
	}
}



